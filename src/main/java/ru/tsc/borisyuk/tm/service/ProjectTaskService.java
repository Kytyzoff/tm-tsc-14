package ru.tsc.borisyuk.tm.service;

import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.service.IProjectTaskService;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskToProjectById(projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(projectId);
    }

    @Override
    public Project removeById(final String projectId) {
        removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

}
