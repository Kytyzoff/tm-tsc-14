package ru.tsc.borisyuk.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
